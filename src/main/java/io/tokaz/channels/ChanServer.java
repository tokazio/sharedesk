package io.tokaz.channels;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import com.github.forax.beautifullogger.Logger;

/**
 *
 * @author romainpetit
 */
public abstract class ChanServer implements Server {

    private static final Logger LOGGER = Logger.getLogger(ChanServer.class);

    private final List<SocketChannel> clients = new CopyOnWriteArrayList<>();
    private ServerSocketChannel serverSocketChannel;
    private boolean terminated;
    private int port;

    @Override
    public <T extends Server> T connect(final int port) throws IOException {
        if (serverSocketChannel == null) {
            this.port = port;
            serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.socket().bind(new InetSocketAddress(port));
            serverSocketChannel.configureBlocking(false);
        }
        return (T) this;
    }

    @Override
    public void start() {
        if (!terminated && serverSocketChannel != null && serverSocketChannel.isOpen()) {
            LOGGER.debug(p -> "Server running on port " + p, Integer.toString(port));
            while (!terminated) {
                try {
                    SocketChannel socketChannel = serverSocketChannel.accept();
                    if (socketChannel != null) {
                        LOGGER.debug(a -> "New client: " + a, socketChannel.getRemoteAddress());
                        socketChannel.socket().setKeepAlive(true);
                        clients.add(socketChannel);
                        //todo display client list, with display name!
                    }
                } catch (IOException ex) {
                    LOGGER.warning("Can't accept connection", ex);
                }
                run(clients);
            }
            stopping();
        } else {
            LOGGER.debug(() -> "You'r not connected, call connect before start");
        }
    }

    private void stopping() {
        removeAllClients();
        try {
            serverSocketChannel.close();
        } catch (IOException ex) {
            LOGGER.warning("Error closing server socket", ex);
        }
        LOGGER.debug(() -> "Server stopped");
    }

    private void removeAllClients() {
        clients.forEach(this::removeClient);
    }

    protected abstract void run(final List<SocketChannel> clients);

    protected void write(final SocketChannel client, final String newData) throws IOException {
        ByteBuffer buf = ByteBuffer.allocate(48);
        buf.clear();
        buf.put(newData.getBytes());
        buf.flip();
        while (buf.hasRemaining()) {
            client.write(buf);
        }
    }

    protected void removeClient(final SocketChannel client) {
        try {
            client.close();
        } catch (IOException ex) {
            LOGGER.warning("Error closing client", ex);
        }
        clients.remove(client);
        LOGGER.debug(() -> "Client disconnected");
    }

    @Override
    public void stop() {
        terminated = true;
    }

    protected void writeToAllClients(final ByteBuffer buf) {
        clients.forEach(client -> {
            try {
                client.write(buf);
                buf.rewind();
            } catch (IOException ex) {
                removeClient(client);
            }
        });
    }
}
