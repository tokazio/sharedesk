package io.tokaz.channels;

import java.io.IOException;

/**
 *
 * @author romainpetit
 */
public interface Client {

    <T extends Client> T connect(final String host, final int port) throws IOException;

    void start();
    
}
