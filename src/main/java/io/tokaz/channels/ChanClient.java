package io.tokaz.channels;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import com.github.forax.beautifullogger.Logger;

/**
 *
 * @author romainpetit
 */
public abstract class ChanClient implements Client {

    private static final Logger LOGGER = Logger.getLogger(ChanClient.class);
    
    private SocketChannel socketChannel;
    private boolean terminated;
    
    @Override
    public <T extends Client> T connect(final String host, final int port) throws IOException {
        if (socketChannel == null) {
            LOGGER.debug(s -> "Connecting to " + s + "...", host + ":" + port);
            socketChannel = SocketChannel.open();
            socketChannel.configureBlocking(false);
            socketChannel.connect(new InetSocketAddress(host, port));
            while (!socketChannel.finishConnect()) {
                LOGGER.debug(() -> "Connecting...");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    LOGGER.warning("Interrupted while connecting...",ex);
                    // Restore interrupted state...
                    Thread.currentThread().interrupt();
                }
            }
            socketChannel.socket().setKeepAlive(true);
            LOGGER.debug(s -> "Connected to " + s, host + ":" + port);
        }
        return (T) this;
    }

    protected abstract void handle(final ByteBuffer buffer);

    protected abstract void onTerminated();

    @Override
    public void start() {
        if (socketChannel != null && socketChannel.isOpen()) {
            while (!terminated) {
                try {
                    final ByteBuffer buf = read();
                    if (buf != null) {
                        handle(buf);
                    }
                } catch (IOException ex) {
                    LOGGER.warning("Error reading, terminating...", ex);
                    terminated = true;
                }
            }
            try {
                socketChannel.close();
            } catch (IOException ex) {
                LOGGER.error("Error closing socket",ex);
            }
            onTerminated();            
        } else {
            LOGGER.debug(()->"You're not connected, call connect before start");
        }
    }
    
    private ByteBuffer read() throws IOException {
        final ByteBuffer b2 = ByteBuffer.allocate(4);
        final int r = socketChannel.read(b2);
        if (r > 0) {
            b2.flip();
            final int len = b2.getInt();
            final ByteBuffer b = ByteBuffer.allocate(len*Integer.BYTES);
            while (b.hasRemaining()) {
                socketChannel.read(b);
            }            
            b.flip();
            return b;
        }
        return null;
    }
}
