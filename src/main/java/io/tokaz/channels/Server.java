package io.tokaz.channels;

import java.io.IOException;

/**
 *
 * @author romainpetit
 */
public interface Server {

    <T extends Server> T connect(final int port) throws IOException;

    //int getPort();

    void start();

    void stop();
    
}
