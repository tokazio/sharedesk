package io.tokaz.sharedesk.dfmirage;

import com.sun.jna.NativeLong;
import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * 2005.11.21<br>
 * <i>native declaration : line 187</i><br>
 * This file was autogenerated by <a href="http://jnaerator.googlecode.com/">JNAerator</a>,<br>
 * a tool written by <a href="http://ochafik.com/">Olivier Chafik</a> that <a href="http://code.google.com/p/jnaerator/wiki/CreditsAndLicense">uses a few opensource projects.</a>.<br>
 * For help, please visit <a href="http://nativelibs4java.googlecode.com/">NativeLibs4Java</a> , <a href="http://rococoa.dev.java.net/">Rococoa</a>, or <a href="http://jna.dev.java.net/">JNA</a>.
 */
public class Esc_dmf_Qvi_IN extends Structure {
    public NativeLong cbSize;
    public NativeLong app_actual_version;
    public NativeLong display_minreq_version;
    /// reserved. must be 0.
    public NativeLong connect_options;

    public Esc_dmf_Qvi_IN() {
        super();
        initFieldOrder();
    }

    /// @param connect_options reserved. must be 0.
    public Esc_dmf_Qvi_IN(NativeLong cbSize, NativeLong app_actual_version, NativeLong display_minreq_version, NativeLong connect_options) {
        super();
        this.cbSize = cbSize;
        this.app_actual_version = app_actual_version;
        this.display_minreq_version = display_minreq_version;
        this.connect_options = connect_options;
        initFieldOrder();
    }

    @Override
    protected List<String> getFieldOrder() {
        return Arrays.asList(new String[]{"cbSize", "app_actual_version", "display_minreq_version", "connect_options"});
    }

    protected void initFieldOrder() {
        setFieldOrder(new String[]{"cbSize", "app_actual_version", "display_minreq_version", "connect_options"});
    }

    public static class ByReference extends Esc_dmf_Qvi_IN implements Structure.ByReference {

    }

    ;

    public static class ByValue extends Esc_dmf_Qvi_IN implements Structure.ByValue {

    }

    ;
}
