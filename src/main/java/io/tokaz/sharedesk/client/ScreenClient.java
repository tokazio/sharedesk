package io.tokaz.sharedesk.client;

import com.github.forax.beautifullogger.Logger;
import io.tokaz.channels.ChanClient;
import io.tokaz.sharedesk.Data;
import java.nio.ByteBuffer;

/**
 *
 * @author romainpetit
 */
class ScreenClient extends ChanClient {

    private static final Logger LOGGER = Logger.getLogger(ScreenClient.class);
    
    private final ClientForm form;

    ScreenClient(final String service, final String name, final boolean scaled) {
        form = new ClientForm("ShareDesk - " + service + " - " + name, scaled);
    }

    @Override
    protected void handle(final ByteBuffer buffer) {
        final Data data = readData(buffer);
        if (data != null) {
            form.getPanel().update(data);
            sleep();
        }
    }

    private void sleep() {
        try {
            Thread.sleep(250);
        } catch (InterruptedException ex) {
            LOGGER.warning("Interrupted while updating screen", ex);
            // Restore interrupted state...
            Thread.currentThread().interrupt();
        }
    }

    private Data readData(final ByteBuffer buffer) {
        final int[] a = new int[buffer.capacity() / Integer.BYTES];
        buffer.asIntBuffer().get(a);
        return Data.fromPackedArray(a);
    }

    @Override
    protected void onTerminated() {
        form.setVisible(false);
        System.exit(0);
    }

}
