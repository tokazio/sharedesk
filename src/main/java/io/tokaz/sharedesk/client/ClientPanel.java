package io.tokaz.sharedesk.client;

import io.tokaz.sharedesk.Data;
import java.awt.Graphics;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import io.tokaz.sharedesk.commons.Curseur;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

/**
 *
 * @author Tokazio
 */
class ClientPanel extends JPanel {

    private Data data;
    private final ClientForm form;

    private int xdec = 0;
    private int ydec = 0;

    private transient BufferedImage image;
    private int[] tmp;
    
    ClientPanel(final ClientForm form) {
	super();
	this.form = form;
	super.setDoubleBuffered(false);
    }
    
    private void initImage(final Data data){
        if(image==null){
            image = new BufferedImage(data.screenW(), data.screenH(), BufferedImage.TYPE_INT_ARGB);
            tmp = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
        }
    }

    @Override
    public void paintComponent(final Graphics graphics) {
        if(this.data==null || this.data.screenW()==0 || this.data.screenH()==0){
            return;
        }
	// Get server mouse position
	int x = this.data.mouseX();
	int y = this.data.mouseY();
	// compute scaled mouse position

	int xlocal;
	int ylocal;
	
	double xcm = x / (double) this.data.screenW();
	double ycm = y / (double) this.data.screenH();

	if (!form.isScaled() && this.data.screenW() > getWidth()) {
	    xlocal = (int) (getWidth() * xcm);
	} else {
	    xlocal = (int) (this.data.screenW() * xcm);
	}

	if (!form.isScaled() && this.data.screenH() > getHeight()) {
	    ylocal = (int) (getHeight() * ycm);
	} else {
	    ylocal = (int) (this.data.screenH() * ycm);
	}

	int wlocal = image.getWidth(null) - getWidth();
	int hlocal = image.getHeight(null) - getHeight();

        System.arraycopy(data.dataArray(), 0, tmp, 0, data.dataArray().length);
        
	// paint screen
	if (image != null) {
	    if (form.isScaled()) {
                graphics.drawImage(image, 0, 0, getWidth(), getHeight(), null);
	    } else {
		if (this.data.screenW() > getWidth()) {
		    xdec = (int) (-wlocal * xcm);
		} else {
		    xdec = 0;
		}
		if (this.data.screenH() > getHeight()) {
		    ydec = (int) (-hlocal * ycm);
		} else {
		    ydec = 0;
		}
		graphics.drawImage(image, xdec, ydec, null);
	    }
	}
	// paint mouse
        Curseur.draw(graphics, xlocal, ylocal);
    }

    void update(final Data data) {
	this.data = data;
        initImage(data);
        SwingUtilities.invokeLater(() ->    paintImmediately(0, 0, getWidth(), getHeight()));        
    }

    ClientForm getForm() {
	return form;
    }

    int getXdec() {
	return xdec;
    }

    int getYdec() {
	return ydec;
    }
}
