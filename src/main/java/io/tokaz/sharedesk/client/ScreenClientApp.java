package io.tokaz.sharedesk.client;

import static com.github.forax.beautifullogger.Logger.Level.DEBUG;
import com.github.forax.beautifullogger.LoggerConfig;
import java.io.IOException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import java.net.ConnectException;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 *
 * @author romainpetit
 */
public class ScreenClientApp {

    private static final String SERVICE = "service";
    
    public static void main(String[] args) {
        final Options options = configParameters();
        final CommandLineParser parser = new DefaultParser();
        try {
            connect(parser.parse(options, args));
        } catch (ParseException ex) {
            final HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("ShareDesk client", options, true);
        }
    }

    private static void connect(final CommandLine line) {
        
        final boolean debug = line.hasOption("debug");
        LoggerConfig.fromPackage("com.esyo.sharedesk.channels").update(opt -> {
            opt.enable(debug);
            opt.level(DEBUG,true);
                });
        
        final String host = line.getOptionValue("host", "localhost");
        final int port = Integer.parseInt(line.getOptionValue("port", "4096"));
        try {
            new ScreenClient(line.getOptionValue(SERVICE), line.getOptionValue("name"), line.hasOption("scale")).connect(host, port).start();
        } catch (ConnectException ex) {
            showMessage("Can't connect to " + host + ":" + port);
        } catch(IOException ex){
            showMessage("Erreur "+ex.getMessage());
        }
    }

    private static void showMessage(final String msg) {
        SwingUtilities.invokeLater(() ->
            JOptionPane.showMessageDialog(null, msg, "ERREUR", JOptionPane.ERROR_MESSAGE)
        );
    }

    private static Options configParameters() {

        final Option serviceOpt = Option.builder("s")
                .longOpt(SERVICE) //
                .desc("Service name. Required.")
                .hasArg(true)
                .argName(SERVICE)
                .required(true)
                .build();

        final Option hostOpt = Option.builder("h")
                .longOpt("host") //
                .desc("Host name/ip. 'localhost' as default.")
                .hasArg(true)
                .argName("host")
                .required(false)
                .build();

        final Option nameOpt = Option.builder("u")
                .longOpt("name")
                .desc("User name. Required.")
                .hasArg(true)
                .argName("name")
                .required(true)
                .build();

        final Option portOpt = Option.builder("p")
                .longOpt("port")
                .desc("Port. 4096 as default.")
                .hasArg(true)
                .required(false)
                .build();

        final Option scaleOpt = Option.builder("a")
                .longOpt("scale")
                .desc("Scale server screen.")
                .hasArg(false)
                .required(false)
                .build();

        final Option debugOpt = Option.builder("d")
                .longOpt("debug")
                .desc("Debug mode.")
                .hasArg(false)
                .required(false)
                .build();

        
        final Options options = new Options();

        options.addOption(serviceOpt);
        options.addOption(nameOpt);
        options.addOption(portOpt);
        options.addOption(scaleOpt);
        options.addOption(hostOpt);
        options.addOption(debugOpt);

        return options;
    }
}
