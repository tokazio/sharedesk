package io.tokaz.sharedesk.client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

/**
 *
 * @author tokazio
 */
class ClientForm extends JFrame {
    
    private static final long serialVersionUID = -5518860281358876130L;

    private final boolean scaled;
    private final ClientPanel panel;

    ClientForm(final String title, final boolean scaled) {
        this.scaled = scaled;
	super.setTitle(title);
	super.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	super.addMouseListener(new MouseAdapter() {

	    @Override
	    public void mouseClicked(MouseEvent e) {
		//todo client.click(new Point((int) (e.getX() - getPanel().getXdec()), (int) (e.getY() - getPanel().getYdec())));
	    }
	});

	panel = new ClientPanel(this);
        panel.setPreferredSize(new Dimension(800,600));
	super.add(panel, BorderLayout.CENTER);
        
        super.pack();
        super.setVisible(true);
    }

    ClientPanel getPanel() {
	return panel;
    }
    
    boolean isScaled(){
        return scaled;
    }
}
