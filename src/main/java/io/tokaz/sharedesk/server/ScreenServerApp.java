package io.tokaz.sharedesk.server;

import com.github.forax.beautifullogger.LoggerConfig;
import java.io.IOException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import com.github.forax.beautifullogger.Logger;
import static com.github.forax.beautifullogger.Logger.Level.DEBUG;

/**
 *
 * @author romainpetit
 */
public class ScreenServerApp {

    private static final Logger LOGGER = Logger.getLogger(ScreenServerApp.class);

    private static final String SERVICE_OPT = "service";
    private static final String INTERFACE_OPT = "interface";
    
    private final String serviceName;//required as arg
    private final String name;//required as arg
    private final int port;//see default in the constructor
    private final int rate;//see default in the constructor
    private final int screen;//see default in the constructor
    private final boolean debug;
    private final String interfaceName;//todo

    private final ScreenServer server;

    public static void main(String[] args) throws IOException {
        final Options options = configParameters();
        final CommandLineParser parser = new DefaultParser();
        try {
            new ScreenServerApp(parser.parse(options, args));
        } catch (ParseException ex) {
            final HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("ShareDesk server", options, true);
        }
    }

    private static Options configParameters() {

        final Option serviceOpt = Option.builder("s")
                .longOpt(SERVICE_OPT)
                .desc("Service name. Required.")
                .hasArg(true)
                .argName(SERVICE_OPT)
                .required(true)
                .build();

        final Option interfaceOpt = Option.builder("i")
                .longOpt(INTERFACE_OPT)
                .desc("Interface filter")
                .hasArg(true)
                .argName(INTERFACE_OPT)
                .required(false)
                .build();

        final Option nameOpt = Option.builder("u")
                .longOpt("name")
                .desc("User name. Required.")
                .hasArg(true)
                .argName("name")
                .required(true)
                .build();

        final Option portOpt = Option.builder("p")
                .longOpt("port")
                .desc("Port. 4096 as default.")
                .hasArg(true)
                .required(false)
                .build();

        final Option rateOpt = Option.builder("r")
                .longOpt("rate")
                .desc("Screen shooting rate. 240 as default.")
                .hasArg(true)
                .required(false)
                .build();

        final Option screenOpt = Option.builder("c")
                .longOpt("screen")
                .desc("Screen number. 0 as default.")
                .hasArg(true)
                .required(false)
                .build();

        final Option debugOpt = Option.builder("d")
                .longOpt("debug")
                .desc("Debug mode.")
                .hasArg(false)
                .required(false)
                .build();

        final Options options = new Options();
        options.addOption(serviceOpt);
        options.addOption(nameOpt);
        options.addOption(portOpt);
        options.addOption(rateOpt);
        options.addOption(screenOpt);
        options.addOption(interfaceOpt);
        options.addOption(debugOpt);
        return options;
    }

    private ScreenServerApp(final CommandLine line) throws IOException {

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                LOGGER.debug(()->"Shutdown Hook");
                server.stop();
            }
        });

        serviceName = line.getOptionValue(SERVICE_OPT);
        name = line.getOptionValue("name");
        port = Integer.parseInt(line.getOptionValue("port", "4096"));
        rate = Integer.parseInt(line.getOptionValue("rate", "240"));
        screen = Integer.parseInt(line.getOptionValue("screen", "0"));
        debug = line.hasOption("debug");
        interfaceName = line.getOptionValue(INTERFACE_OPT, "Intel(R) Ethernet Connection");

        LoggerConfig.fromClass("io.tokaz.sharedesk.server.ScreenServerApp").update(opt -> {
            LOGGER.info(()->"Debug mode (-d): " + debug);
            opt.enable(debug);
            opt.level(DEBUG,true);
                });
        
        
        LOGGER.info(()->"Service name (-s): " + serviceName);
        LOGGER.debug(()->"User name (-u): " + name);
        LOGGER.debug(()->"Port (-p): " + port);
        LOGGER.debug(()->"Rate (-r): " + rate + " (" + ((int) (1000d / rate)) + "fps)");
        LOGGER.debug(()->"Screen (-c): #" + screen);
        LOGGER.debug(()->"Inteface  name (-i): " + interfaceName);

        server = new ScreenServer(screen, rate).connect(port);
        server.start();       
    }
}
