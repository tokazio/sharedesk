package io.tokaz.sharedesk.server;

import com.github.forax.beautifullogger.Logger;
import io.tokaz.channels.ChanServer;
import io.tokaz.sharedesk.Data;
import io.tokaz.sharedesk.Utils;
import io.tokaz.sharedesk.commons.JavaScreenshot;
import io.tokaz.sharedesk.commons.Screenshot;
import io.tokaz.sharedesk.commons.WinScreenshot;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.Rectangle;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.List;

/**
 *
 * @author romainpetit
 */
class ScreenServer extends ChanServer {

    private static final Logger LOGGER = Logger.getLogger(ScreenServer.class);
    
    private final Screenshot shooter;
    private GraphicsDevice selectedScreen;
    private Rectangle screenBounds;    
    private final int rate;
    private final Data data;

    ScreenServer(final int screen, final int rate) {
        this.rate = rate;
        setScreen(screen);
        if (Utils.isWindows()) {
            shooter = new WinScreenshot(selectedScreen);
            LOGGER.debug(()->"Using JNA on Windows to take screenshot");
        } else {
            shooter = new JavaScreenshot(selectedScreen);
            LOGGER.debug(()->"Using Java to take screenshot");
        }
        data = new Data().screenW(screenWidth()).screenH(screenHeight());
    }

    @Override
    protected void run(final List<SocketChannel> clients) {
        final long s = System.currentTimeMillis();
        doRun();
        final long d = System.currentTimeMillis() - s;
        if (rate - d > 0) {
            try {
                Thread.sleep(rate - d);
            } catch (InterruptedException ex) {
                LOGGER.warning("Interrupted while updating screen",ex);
                // Restore interrupted state...
                Thread.currentThread().interrupt();
            }
        } else {
            LOGGER.debug((a,b)->"Skiped frame (took " + a + "ms that is more than the asked rate of " + b + "ms)",d,rate);
        }
    }

    final void setScreen(final int selectedIndex) {
        for (GraphicsDevice d : GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()) {
            LOGGER.debug((a,b)->"\t" + a + " " + b,d,d.getDefaultConfiguration().getBounds());
        }
        selectedScreen = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()[selectedIndex];
        screenBounds = selectedScreen.getDefaultConfiguration().getBounds();
        LOGGER.debug((a,b)->"Using screen #" + a + "(" + b + ")",selectedIndex,selectedScreen.toString());
    }

    final int screenWidth() {
        return (int) selectedScreen.getDefaultConfiguration().getBounds().getWidth();
    }

    final int screenHeight() {
        return (int) selectedScreen.getDefaultConfiguration().getBounds().getHeight();
    }
   
    private void doRun() {
        try {
            // Mouse
            final PointerInfo info = MouseInfo.getPointerInfo();
            if (info != null && info.getDevice().equals(selectedScreen)) {
                final Point p = info.getLocation();
                int x = (int) (p.getX() - screenBounds.getX());
                int y = (int) (p.getY() - screenBounds.getY());
                x *= x < 0 ? -1 : 1;
                y *= y < 0 ? -1 : 1;
                data.mouseX(x);
                data.mouseY(y);
            }
            //Screenshot
            data.data(shooter.shot(screenBounds));
            // Write to socket
            final int[] send = data.toPackedArray();
            
            final ByteBuffer buf = ByteBuffer.allocateDirect(Integer.BYTES + send.length * Integer.BYTES);
            buf.asIntBuffer().put(send.length).put(send);
            writeToAllClients(buf);            
        } catch (HeadlessException ioe) {
            LOGGER.debug("Error screenshooting in headless environment", ioe);
            stop();
        }
    }


}
