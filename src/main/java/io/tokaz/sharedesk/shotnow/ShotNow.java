package io.tokaz.sharedesk.shotnow;

import io.tokaz.sharedesk.commons.WinScreenshot;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

public class ShotNow {

    public ShotNow() {
        try {
            for (GraphicsDevice d : GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()) {
                System.out.println("#" + d + " " + d.getDefaultConfiguration().getBounds());
                GraphicsConfiguration[] gc =
                        d.getConfigurations();
                for (int i = 0; i < gc.length; i++) {
                    Rectangle gcBounds = gc[i].getBounds();
                    System.out.println("\t" + gcBounds.x + "," + gcBounds.y);
                }
            }
            //GraphicsDevice screen = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()[1];
            System.out.println("Max: " +
                    GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds() + " center:" +
                    GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint());

            //GraphicsDevice screen = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
            GraphicsDevice screen = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()[1];
            Robot robot = new Robot(screen);
            Rectangle rect = new Rectangle(1920, -634, 3000, 1286);//screenBounds
            //BufferedImage img = robot.createScreenCapture(rect);
            BufferedImage img = WinScreenshot.shotAsImage(rect);

            ImageIO.write(img, "jpg", new File("shot.jpg"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String... args) {
        new ShotNow();
    }


}
