package io.tokaz.sharedesk;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Properties;
import com.github.forax.beautifullogger.Logger;

/**
 *
 * @author rpetit
 */
public final class Utils {

    private static final Logger LOGGER = Logger.getLogger(Utils.class);
    private static InetAddress ip;

    //hide
    private Utils() {
        super();
    }

    public static InetAddress getIp() throws UnknownHostException, SocketException {
        if (ip == null) {
            final Properties props = new Properties();
            //todo use -i
            final String interf = props.getProperty("interface", "Intel(R) Ethernet Connection");
            LOGGER.info(s->"Using interface '" + s + "'...",interf);
            final Enumeration<NetworkInterface> enums = NetworkInterface.getNetworkInterfaces();
            while (enums.hasMoreElements()) {
                final NetworkInterface nif = enums.nextElement();
                if (nif.getDisplayName().contains(interf)&&nif.getInetAddresses().hasMoreElements()) {
                        ip = nif.getInetAddresses().nextElement();
                        LOGGER.info(s->"Found ip '" + s + "'",ip);
                        break;
                    }
                
            }
            if (ip == null) {
                ip = InetAddress.getLocalHost();
                LOGGER.info(s->"Use default ip '" + s + "'",ip);
            }
        }
        return ip;
    }
    
    public static boolean isWindows() {
        return System.getProperty("os.name").toLowerCase().contains("win");
    }
}
