package io.tokaz.sharedesk;

import java.io.Serializable;
import me.lemire.integercompression.differential.IntegratedIntCompressor;

/**
 *
 * @author romainpetit
 */
public class Data implements Serializable{

    private static final IntegratedIntCompressor IIC = new IntegratedIntCompressor();

    private int mouseX;
    private int mouseY;
    private int screenW;
    private int screenH;
    private int dataType;
    private int[] dataArray;

    public Data mouse(final int x, final int y) {
        mouseX = x;
        mouseY = y;
        return this;
    }

    public Data screen(final int w, final int h) {
        screenW = w;
        screenH = h;
        return this;
    }

    public Data diff(final int[] data) {
        dataType = 1;
        this.dataArray = data;
        return this;
    }

    public Data data(final int[] data) {
        dataType = 0;
        this.dataArray = data;
        return this;
    }

    public int[] toArray() {
        if (dataArray != null) {
            final int[] ret = new int[dataArray.length + 6];
            ret[0] = mouseX;
            ret[1] = mouseY;
            ret[2] = screenW;
            ret[3] = screenH;
            ret[4] = dataType;
            ret[5] = dataArray.length;
            System.arraycopy(dataArray, 0, ret, 6, dataArray.length);
            return ret;
        }
        return new int[0];
    }

    public int[] toPackedArray() {
        return IIC.compress(toArray());
    }

    public static Data fromArray(final int[] array) {
        return new Data().read(array);
    }

    private Data read(final int[] array) {
        mouseX = array[0];
        mouseY = array[1];
        screenW = array[2];
        screenH = array[3];
        dataType = array[4];
        dataArray = new int[array[5]];
        System.arraycopy(array, 6, dataArray, 0, dataArray.length);
        return this;
    }

    public static Data fromPackedArray(final int[] array) {
        return new Data().read(IIC.uncompress(array));
    }

    @Override
    public String toString() {
        return new StringBuilder().append("Data{mouseX=").append(mouseX).append(", mouseY=").append(mouseY).append(", screenW=").append(screenW).append(", screenH=").append(screenH).append(", dataType=").append(dataType).append(", dataLen=").append(dataArray.length).append("}").toString();
    }

    public Data screenW(final int w) {
        screenW = w;
        return this;
    }

    public Data screenH(final int h) {
        screenH = h;
        return this;
    }

    public int screenH() {
        return screenH;
    }

    public int screenW() {
        return screenW;
    }

    public int[] dataArray() {
        return dataArray;
    }

    public int mouseX() {
        return mouseX;
    }

    public int mouseY() {
        return mouseY;
    }

    public void mouseY(final int y) {
        mouseY = y;
    }

    public void mouseX(final int x) {
        mouseX = x;
    }

}
