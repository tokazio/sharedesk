package io.tokaz.sharedesk.commons;

import com.sun.jna.Native;
import com.sun.jna.NativeLong;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinGDI;
import com.sun.jna.platform.win32.WinNT;
import com.sun.jna.platform.win32.WinUser;
import com.sun.jna.win32.W32APIOptions;
import io.tokaz.sharedesk.dfmirage.CHANGES_BUF;
import io.tokaz.sharedesk.dfmirage.CHANGES_RECORD;
import io.tokaz.sharedesk.dfmirage.DfmirageLibrary;

import java.awt.*;
import java.awt.image.*;

import static com.sun.jna.platform.win32.WinUser.*;
import static io.tokaz.sharedesk.dfmirage.DfmirageLibrary.dmf_escape.dmf_esc_usm_pipe_map;

interface GDI32 extends com.sun.jna.platform.win32.GDI32,
        com.sun.jna.platform.win32.WinGDI,
        com.sun.jna.platform.win32.WinDef {

    GDI32 INSTANCE =
            (GDI32) Native.loadLibrary(GDI32.class);

    /*boolean BitBlt(HDC hdcDest, int nXDest, int nYDest,
            int nWidth, int nHeight, HDC hdcSrc,
            int nXSrc, int nYSrc, int dwRop);*/

    HDC GetDC(HWND hWnd);

    int ExtEscape(
            HDC hdc,
            int iEscape,
            int cjInput,
            CHANGES_BUF lpInData,
            int cjOutput,
            CHANGES_BUF lpOutData
    );

    boolean GetDIBits(HDC dc, HBITMAP bmp, int startScan, int scanLines,
                      byte[] pixels, BITMAPINFO bi, int usage);

    boolean GetDIBits(HDC dc, HBITMAP bmp, int startScan, int scanLines,
                      short[] pixels, BITMAPINFO bi, int usage);

    boolean GetDIBits(HDC dc, HBITMAP bmp, int startScan, int scanLines,
                      int[] pixels, BITMAPINFO bi, int usage);
    //int SRCCOPY = 0xCC0020;
}

public class WinScreenshot implements Screenshot {

    DfmirageLibrary mirage = DfmirageLibrary.INSTANCE;

    public WinScreenshot(final GraphicsDevice selectedScreen) {
        super();
        //todo use selectedScreen
    }

    public static void main(String[] args) {

        WinUser.MONITORENUMPROC a = new WinUser.MONITORENUMPROC() {

            @Override
            public int apply(WinUser.HMONITOR hMonitor, WinDef.HDC hdc, WinDef.RECT rect, WinDef.LPARAM lparam) {
                System.out.println(rect);
                return 1;
            }
        };

        System.out.println(USER.EnumDisplayMonitors(null, null, a, new WinDef.LPARAM(0)).booleanValue());

        CHANGES_BUF b = new CHANGES_BUF(new NativeLong(10), new CHANGES_RECORD[10]);

        int r = GDI32.INSTANCE.ExtEscape(GDI.GetDC(null), dmf_esc_usm_pipe_map, 0, null, 10, b);
        if (r == 0) {
            System.out.println(">>>ERROR");
        }
        System.out.println(">>>" + r);

        System.out.println(a);

        WinDef.HWND desktopWindow = User32.INSTANCE.GetDesktopWindow();


        WinDef.HWND hwnd = User32.INSTANCE.GetAncestor(desktopWindow, WinUser.GA_ROOT);

        //WinDef.HWND hwnd = User32.INSTANCE.GetActiveWindow();//new WinDef.HWND();//User32.INSTANCE.GetDesktopWindow()

        WinUser.HMONITOR hMon = User32.INSTANCE.MonitorFromWindow(hwnd, WinUser.MONITOR_DEFAULTTOPRIMARY);

        WinUser.MONITORINFO info = new WinUser.MONITORINFO();
        User32.INSTANCE.GetMonitorInfo(hMon, info).booleanValue();
        System.out.println(info.rcMonitor + " (" + info.rcWork + ")");


    }

    public static int[] getScreenshot(final Rectangle bounds) {
        WinDef.HDC windowDC = GDI.GetDC(USER.GetDesktopWindow());
        WinDef.HBITMAP outputBitmap =
                GDI.CreateCompatibleBitmap(windowDC,
                        bounds.width, bounds.height);
        try {
            WinDef.HDC blitDC = GDI.CreateCompatibleDC(windowDC);
            try {
                WinNT.HANDLE oldBitmap =
                        GDI.SelectObject(blitDC, outputBitmap);
                try {
                    GDI.BitBlt(blitDC,
                            0, 0, bounds.width, bounds.height,
                            windowDC,
                            bounds.x, bounds.y,
                            GDI32.SRCCOPY);
                } finally {
                    GDI.SelectObject(blitDC, oldBitmap);
                }
                WinGDI.BITMAPINFO bi = new WinGDI.BITMAPINFO(40);
                bi.bmiHeader.biSize = 40;
                boolean ok =
                        GDI.GetDIBits(blitDC, outputBitmap, 0, bounds.height,
                        (byte[]) null, bi, WinGDI.DIB_RGB_COLORS);
                if (ok) {
                    WinGDI.BITMAPINFOHEADER bih = bi.bmiHeader;
                    bih.biHeight = -Math.abs(bih.biHeight);
                    bi.bmiHeader.biCompression = 0;
                    return data(blitDC, outputBitmap, bi);
                } else {
                    return null;
                }
            } finally {
                GDI.DeleteObject(blitDC);
            }
        } finally {
            GDI.DeleteObject(outputBitmap);
        }
    }

    private static int[] data(WinDef.HDC blitDC,
            WinDef.HBITMAP outputBitmap,
            WinGDI.BITMAPINFO bi) {
        WinGDI.BITMAPINFOHEADER bih = bi.bmiHeader;
        int height = Math.abs(bih.biHeight);
        final ColorModel cm;
        final DataBuffer buffer;
        final WritableRaster raster;
        int strideBits =
                (bih.biWidth * bih.biBitCount);
        int strideBytesAligned =
                (((strideBits - 1) | 0x1F) + 1) >> 3;
        final int strideElementsAligned;
        switch (bih.biBitCount) {
            case 16:
                strideElementsAligned = strideBytesAligned / 2;
                cm = new DirectColorModel(16, 0x7C00, 0x3E0, 0x1F);
                buffer =
                        new DataBufferUShort(strideElementsAligned * height);
                raster =
                        Raster.createPackedRaster(buffer,
                        bih.biWidth, height,
                        strideElementsAligned,
                        ((DirectColorModel) cm).getMasks(),
                        null);
                break;
            case 32:
                strideElementsAligned = strideBytesAligned / 4;
                cm = new DirectColorModel(32, 0xFF0000, 0xFF00, 0xFF);
                buffer =
                        new DataBufferInt(strideElementsAligned * height);
                raster =
                        Raster.createPackedRaster(buffer,
                        bih.biWidth, height,
                        strideElementsAligned,
                        ((DirectColorModel) cm).getMasks(),
                        null);
                break;
            default:
                throw new IllegalArgumentException("Unsupported bit count: " + bih.biBitCount);
        }
        final boolean ok;
        switch (buffer.getDataType()) {
            case DataBuffer.TYPE_INT: {
                int[] pixels = ((DataBufferInt) buffer).getData();
                ok = GDI.GetDIBits(blitDC, outputBitmap, 0, raster.getHeight(), pixels, bi, 0);
            }
            break;
            case DataBuffer.TYPE_USHORT: {
                short[] pixels = ((DataBufferUShort) buffer).getData();
                ok = GDI.GetDIBits(blitDC, outputBitmap, 0, raster.getHeight(), pixels, bi, 0);
            }
            break;
            default:
                throw new AssertionError("Unexpected buffer element type: " + buffer.getDataType());
        }
        if (ok) {
            return ((DataBufferInt) raster.getDataBuffer()).getData();
        } else {
            return null;
        }
    }

    public static BufferedImage shotAsImage(final Rectangle bounds) {

        WinDef.HDC dcScreen = GDI.GetDC(null);//USER.GetDesktopWindow());

        int x = USER.GetSystemMetrics(SM_XVIRTUALSCREEN);  //left (e.g. -1024)
        int y = USER.GetSystemMetrics(SM_YVIRTUALSCREEN);  //top (e.g. -34)
        int cx = USER.GetSystemMetrics(SM_CXVIRTUALSCREEN); //entire width (e.g. 2704)
        int cy = USER.GetSystemMetrics(SM_CYVIRTUALSCREEN); //entire height (e.g. 1050)

        System.out.println(x + "," + y + " " + cx + "," + cy);

        WinDef.HBITMAP bmpTarget =
                GDI.CreateCompatibleBitmap(dcScreen, cx, cy);
        try {
            WinDef.HDC dcTarget = GDI.CreateCompatibleDC(dcScreen);
            try {
                WinNT.HANDLE oldBitmap = GDI.SelectObject(dcTarget, bmpTarget);
                try {
                    GDI.BitBlt(dcTarget,
                            0, 0, cx, cy,
                            dcScreen,
                            x, y,
                            GDI32.SRCCOPY | (0x00CC0020 | 0x40000000));
                } finally {
                    GDI.SelectObject(dcTarget, oldBitmap);
                }


                WinGDI.BITMAPINFO bi = new WinGDI.BITMAPINFO(40);
                bi.bmiHeader.biSize = 40;
                bi.bmiHeader.biWidth = cx;
                bi.bmiHeader.biHeight = cy;
                boolean ok =
                        GDI.GetDIBits(dcTarget, bmpTarget, y, cy,
                                (byte[]) null, bi, WinGDI.DIB_RGB_COLORS);
                if (ok) {
                    WinGDI.BITMAPINFOHEADER bih = bi.bmiHeader;
                    bih.biHeight = -Math.abs(bih.biHeight);
                    bi.bmiHeader.biCompression = 0;
                    return bufferedImageFromBitmap(dcTarget, bmpTarget, bi);
                } else {
                    return null;
                }
            } finally {
                GDI.DeleteObject(dcTarget);
            }
        } finally {
            GDI.DeleteObject(bmpTarget);
        }
    }

    private static BufferedImage bufferedImageFromBitmap(WinDef.HDC blitDC,
                                                         WinDef.HBITMAP outputBitmap,
                                                         WinGDI.BITMAPINFO bi) {
        WinGDI.BITMAPINFOHEADER bih = bi.bmiHeader;
        int height = Math.abs(bih.biHeight);
        final ColorModel cm;
        final DataBuffer buffer;
        final WritableRaster raster;
        int strideBits =
                (bih.biWidth * bih.biBitCount);
        int strideBytesAligned =
                (((strideBits - 1) | 0x1F) + 1) >> 3;
        final int strideElementsAligned;
        switch (bih.biBitCount) {
            case 16:
                strideElementsAligned = strideBytesAligned / 2;
                cm = new DirectColorModel(16, 0x7C00, 0x3E0, 0x1F);
                buffer =
                        new DataBufferUShort(strideElementsAligned * height);
                raster =
                        Raster.createPackedRaster(buffer,
                        bih.biWidth, height,
                        strideElementsAligned,
                        ((DirectColorModel) cm).getMasks(),
                        null);
                break;
            case 32:
                strideElementsAligned = strideBytesAligned / 4;
                cm = new DirectColorModel(32, 0xFF0000, 0xFF00, 0xFF);
                buffer =
                        new DataBufferInt(strideElementsAligned * height);
                raster =
                        Raster.createPackedRaster(buffer,
                        bih.biWidth, height,
                        strideElementsAligned,
                        ((DirectColorModel) cm).getMasks(),
                        null);
                break;
            default:
                throw new IllegalArgumentException("Unsupported bit count: " + bih.biBitCount);
        }
        final boolean ok;
        switch (buffer.getDataType()) {
            case DataBuffer.TYPE_INT: {
                int[] pixels = ((DataBufferInt) buffer).getData();
                ok = GDI.GetDIBits(blitDC, outputBitmap, 0, raster.getHeight(), pixels, bi, 0);
            }
            break;
            case DataBuffer.TYPE_USHORT: {
                short[] pixels = ((DataBufferUShort) buffer).getData();
                ok = GDI.GetDIBits(blitDC, outputBitmap, 0, raster.getHeight(), pixels, bi, 0);
            }
            break;
            default:
                throw new AssertionError("Unexpected buffer element type: " + buffer.getDataType());
        }
        if (ok) {
            return new BufferedImage(cm, raster, false, null);
        } else {
            return null;
        }
    }

    private static final User32 USER = User32.INSTANCE;
    private static final GDI32 GDI = GDI32.INSTANCE;

    @Override
    public int[] shot(Rectangle bounds) {
        return getScreenshot(bounds);
    }

    @Override
    public BufferedImage shotImage(Rectangle bounds) {
        return shotAsImage(bounds);
    }
}

interface User32 extends com.sun.jna.platform.win32.User32 {

    User32 INSTANCE = (User32) Native.loadLibrary(User32.class, W32APIOptions.UNICODE_OPTIONS);

    //com.sun.jna.platform.win32.WinDef.HWND GetDesktopWindow();
}
