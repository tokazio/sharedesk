package io.tokaz.sharedesk.commons;

import java.awt.Point;
import java.io.Serializable;

/**
 *
 * @author rpetit
 */
public class Click implements Serializable {

    private final Point point;
    private final String displayName;
    
    public Click(final Point point, final String displayName){
        this.point = point;
        this.displayName = displayName;
    }
    
    public Point point(){
        return this.point;
    }
    
    public String displayName(final String defaultName){
        return this.displayName==null || this.displayName.isEmpty() || this.displayName.equals("?") ? defaultName : this.displayName;
    }
}
