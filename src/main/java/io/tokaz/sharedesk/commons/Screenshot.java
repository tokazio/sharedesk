package io.tokaz.sharedesk.commons;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * @author romainpetit
 */
public interface Screenshot {

    int[] shot(final Rectangle bounds);

    BufferedImage shotImage(Rectangle bounds);
}
