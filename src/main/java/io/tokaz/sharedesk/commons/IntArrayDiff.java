package io.tokaz.sharedesk.commons;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author romainpetit
 */
public final class IntArrayDiff {
    
    //hide
    private IntArrayDiff(){
        super();
    }
    
    public static int[] diff(final int[] olds, final int[] news){
        if(olds==null){
            return new int[0];
        }
        if(olds.length!=news.length){
            throw new IllegalArgumentException("Arrays lengths differs");            
        }
        final List<Integer> diff = new ArrayList<>();
        for(int i=0;i<olds.length;i++){
            if(news[i]!=olds[i]){
                diff.add(i);
                diff.add(news[i]);
            }
        }
        return diff.stream().mapToInt(i->i).toArray();
    }
    
    public static void merge(final int[] values, int[] into){
        for(int i=0;i<values.length;i+=2){
            into[values[i]] = values[i+1];
        }
    }
}
