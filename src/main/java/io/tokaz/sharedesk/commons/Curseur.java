package io.tokaz.sharedesk.commons;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

/**
 *
 * @author Tokazio
 *
 */
public class Curseur {

    //Sonar hide constructor
    private Curseur() {
        super();
    }

    public static void draw(final Graphics graphics, final int x, final int y) {
	final Polygon p = new Polygon();
	p.addPoint(x, y - 1);
	p.addPoint(x, y + 14);
	p.addPoint(x + 3, y + 9);
	p.addPoint(x + 6, y + 16);
	p.addPoint(x + 8, y + 15);
	p.addPoint(x + 5, y + 9);
	p.addPoint(x + 10, y + 9);
	graphics.setColor(Color.WHITE);
	graphics.fillPolygon(p);
	graphics.setColor(Color.BLACK);
	graphics.drawPolygon(p);
    }
}
