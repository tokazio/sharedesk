package io.tokaz.sharedesk.commons;

import com.github.forax.beautifullogger.Logger;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

/**
 *
 * @author romainpetit
 */
public class JavaScreenshot implements Screenshot {
 
    private static final Logger LOGGER = Logger.getLogger(JavaScreenshot.class);
    private Robot robot;

    public JavaScreenshot(final GraphicsDevice selectedScreen){
        try {
            robot = new Robot(selectedScreen);
        } catch (AWTException ex) {
            LOGGER.error("Error configuring java screen shooting", ex);
        }
    }

    @Override
    public int[] shot(final Rectangle bounds) {
        return ((DataBufferInt) robot.createScreenCapture(bounds).getRaster().getDataBuffer()).getData();
    }

    @Override
    public BufferedImage shotImage(Rectangle bounds) {
        return robot.createScreenCapture(bounds);
    }
}
