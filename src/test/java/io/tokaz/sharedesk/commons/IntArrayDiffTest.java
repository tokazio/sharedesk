package io.tokaz.sharedesk.commons;

import io.tokaz.sharedesk.commons.IntArrayDiff;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;

/**
 *
 * @author romainpetit
 */
public class IntArrayDiffTest {
    
    @Test
    public void testDiff(){
        //given
        final int[] olds = {3,6,4,8};
        final int[] news = {3,9,4,8};
        
        //when
        final int[] diff = IntArrayDiff.diff(olds, news);
        
        //then
        assertThat(diff).containsExactly(1,9);
    }
    
    @Test
    public void testDiff2(){
        //given
        final int[] olds = {3,6,4,8};
        final int[] news = {3,9,5,8};
        
        //when
        final int[] diff = IntArrayDiff.diff(olds, news);
        
        //then
        assertThat(diff).containsExactly(1,9,2,5);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDiffEx(){
        //given
        final int[] olds = {3,6,4,8};
        final int[] news = {3,9,5,8,0};
        
        //when
        final int[] diff = IntArrayDiff.diff(olds, news);
        
        //then
    }
    
    @Test
    public void testMerge(){
        //given
        final int[] olds = {3,6,4,8};
        final int[] diff = {1,9};
        
        //when
        IntArrayDiff.merge(diff, olds);
        
        //then
        assertThat(olds).containsExactly(3,9,4,8);
    }
    
    @Test
    public void testMerge2(){
        //given
        final int[] olds = {3,6,4,8};
        final int[] diff = {1,9,3,0};
        
        //when
        IntArrayDiff.merge(diff, olds);
        
        //then
        assertThat(olds).containsExactly(3,9,4,0);
    }
    
}
