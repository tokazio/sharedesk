# ShareDesk #

Free lightweight screen sharing app (over local network)

### Configuration ###

```
serviceName=Encaissement
interface=Intel(R) Ethernet Connection
displayName=Romain
scale=false
```

* serviceName give a name to your sharing, only client/server with the same serviceName can be connected togethers
* interface can filter the network interface to use (selecting it's first ip)
* displayName show your real name ;)
* scale if true, the server screen is completely displayed in the client frame else the scale is 100% and displaying the server mouse at center

### Server ###

* Show your local IP
* Publish itself on the local network
* Select the screen to share
* See who's connected
* Share screen

### Client ###

* Find published servers on the local network
* See screen shared from the server
* Click to show your mouse position to others
* Manually choose local IP to connect to